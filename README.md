# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This program uses the database from the "icanhazdadjoke" website to output a random dad joke based on the user keyword input

### How do I get set up? ###

run dad.py on the terminal
When promped; enter a keyword that you would like the program to search for.

### Contribution guidelines ###

This is from Colt Steele's Udemy Python bootcamp course. I wrote the code myself as part of the course exercise. 

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact