import requests
from random import choice

url = "https://icanhazdadjoke.com/search"

user_input = input('Let me tell you a joke! Give me a topic: ')
response = requests.get(
	url,
	headers={"Accept":"application/json"},
	params={"term":user_input}
)

data = response.json()

#print(data["joke"])
#print(data["results"])

cnt = len(data["results"])

if cnt == 0:
	print(f"Sorry, I don't have any jokes about {user_input}! Please try again.")
elif cnt == 1:
	rand_choice = choice(data["results"])
	print(f"I've got one joke about {user_input}. Here it is: ")
	print(rand_choice["joke"])
else:
	rand_choice = choice(data["results"])
	print(f"I've got {cnt} jokes about {user_input}. Here's one: ")
	print(rand_choice["joke"])
